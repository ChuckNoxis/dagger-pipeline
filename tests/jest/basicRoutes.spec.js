const { app, httpserver } = require('../../src/server');
const supertest = require('supertest');

const request = supertest(app);
const sleep = require('util').promisify(setTimeout);

/* This file is for testing the basic routes with supertest and jest */
describe('Testing the basic routes :', () => {
  test('Get / should get a 404.', async () => {
    const res = await request.get('/');
    expect(res.status).toBe(404);
  });

  test('Get /api/public/test should get the test message.', async () => {
    const res = await request.get('/api/public/test');
    expect(res.status).toBe(200);
    expect(res.body).toBe('test template service');
  });

  test('Get /api/public/health should get 200.', async () => {
    await sleep(2000);
    const res = await request.get('/api/public/health');
    expect(res.status).toBe(200);
    expect(res.body.uptime).toBeDefined();
    expect(res.body.state).toBe('healthy');
    expect(res.body.message).toBe('OK');
    // Truncate the Date to decimal so the test should execute in less than 1s
    expect(Math.trunc(res.body.timestamp / 1000)).toBe(Math.trunc(Date.now() / 1000));
  });
});

afterAll(async () => {
  await httpserver.close();
});
