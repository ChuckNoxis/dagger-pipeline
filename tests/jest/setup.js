// Set jest Timeout to 10s
jest.setTimeout(10000);

// Disabling console.log and console.debug while running the tests
global.console = {
  ...console,
  log: jest.fn(),
  debug: jest.fn(),
};
