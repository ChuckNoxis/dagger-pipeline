module.exports = {
  verbose: true,
  setupFilesAfterEnv: ['<rootDir>/tests/jest/setup.js'],
  testEnvironment: 'node',
  reporters: ['default', 'jest-junit'],
  collectCoverage: true,
  coverageReporters: [
    'cobertura',
    'text',
    'html',
  ],
  coverageDirectory: 'coverage',
};
