##
## Docker Makefile
##

help: ## This help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m- %-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

ROOT_DIR=`echo ${CURDIR} | tr [:upper:] [:lower:]`
CONTAINER_NAME=$(notdir $(ROOT_DIR))

build:	## Build the image.
	./scripts/updateDockerBuild.sh

run:	## Run the container in detached mode.
	./scripts/deploy.sh

jest:	## Build the Jest image and runs it.
	./scripts/updateDockerBuild.sh jest
	./scripts/deploy.sh jest

start:	build run logs	## Build, run the image and display the logs.

logs:	## Display the logs of the container if it's running.
	@echo "You can quit theses logs with Ctrl+C."
	@docker logs "${CONTAINER_NAME}" -f

stop:	## Stop the container and removes it.
	@echo "Trying to stop container ${CONTAINER_NAME}."
	@docker stop "${CONTAINER_NAME}"

re:	stop build run	## Stop, remove, rebuild and re run the containers.

.PHONY: help build run logs stop re
