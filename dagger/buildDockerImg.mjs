import { connect } from '@dagger.io/dagger';

const ARCH = 'linux/amd64';
const PUBLISH_ADDRESS = 'registry.gitlab.com/chucknoxis/dagger-pipeline/dagger';

connect(async (daggerClient) => {
  // get reference to the local project
  const source = daggerClient.host().directory('.', { exclude: ['node_modules/', 'scripts/'] });

  // build application
  const baseLinux = daggerClient.container({ platform: ARCH })
    .from('node:lts-alpine')
    .withEnvVariable('TZ', 'Europe/Paris')
    .withExec(['apk', 'add', '--no-cache', 'bash', 'curl']);

  // add dependencies to the container
  let baseNode = installNodeDependencies(baseLinux, source);

  // set the container entrypoint
  baseNode = baseNode.withEntrypoint(['npm', 'start'])
    .withExposedPort(3000);

  // publish the container image
  const addr = await baseNode.publish(PUBLISH_ADDRESS);

  console.log(`Published to ${addr}`);
}, { LogOutput: process.stdout });

function installNodeDependencies(container, source) {
  return container
    .withDirectory('/app', source)
    .withWorkdir('/app')
    // mount source code
    .withExec(['npm', 'set', 'progress=false'])
    .withExec(['npm', 'config', 'set', 'depth', '0'])
    .withEnvVariable('CI', 'true')
    .withExec(['npm', 'install']);
}
