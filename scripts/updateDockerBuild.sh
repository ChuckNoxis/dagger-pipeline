#!/usr/bin/env bash

# Theses variables should be the only variables to change
REGISTRY_BASE_URL="registry.gitlab.com"
REGISTRY_GROUP_URL="${REGISTRY_BASE_URL}/chucknoxis/"

# Files to watch to build base.Dockerfile
WATCH_LIST_BASE=("scripts/updateDockerBase.sh" \
            "docker/base.Dockerfile" \
            "package.json" \
            "package-lock.json")

# Files to watch to build dev.Dockerfile and prod.Dockerfile
WATCH_LIST=("scripts/updateDockerBuild.sh" \
            "docker" \
            "tmp-md5-base" \
            "src")

# This function initializes the system prior to main processing
init () {
    set -o nounset    # Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
    ulimit -c 0       # Set Core size to 0

    importDotEnv
    # Call the function cleanupBeforeExit when the signal EXIT is catched
    trap "cleanupBeforeExit" EXIT
}

# This function sources the .env file.
importDotEnv () {
    if [ -f .env ]; then
        export $(sed 's/[[:blank:]]//g; /^#/d' .env | xargs)
    fi
}

# This function is called before exiting the program
cleanupBeforeExit () {
    if [ -f tmp-md5 ]; then
        rm tmp-md5
    fi
    if [ -f tmp-md5-base ]; then
        rm tmp-md5-base
    fi
    loginfo "updateDockerBuild Done"
}

# Format strings for logfunctions
format () {
    echo "$(date +"%d/%m/%y %T") [${1}]";
}
loginfo ()  { echo -e "\033[32m$(format INFO)\033[0m ${@}"; }
lognotice ()  { echo -e "\033[34m$(format NOTICE)\033[0m ${@}"; }
logwarning () { echo -e "\033[33m$(format WARNING)\033[0m ${@}"; }
logerror () { echo -e "\033[31m$(format ERROR)\033[0m ${@}" 1>&2; }

# This function checks if skopeo is present on the system, if not, it exits the script.
checkSkopeoPresence () {
    skopeo help > /dev/null
    if [ $? != 0 ]; then
        logerror "Skopeo is not present, exiting the script now."
        exit 1
    fi
}

# This function checks if docker executor is present on the system, if not, it exits the script.
checkDockerPresence () {
    docker help > /dev/null
    if [ $? != 0 ]; then
        logerror "Docker is not present, exiting the script now."
        exit 1
    fi
}

# This function check if the registry passed in parameter is existing in docker's config.json.
isAuthedToRegistry () {
    AUTHED=0
    REGISTRY_FILE_NAME="config.json"
    DOCKER_CONF_PATH="~/.docker/${REGISTRY_FILE_NAME}"

    if [ -f "${DOCKER_CONF_PATH}" ]; then
        cat ${DOCKER_CONF_PATH} | grep ${1} > /dev/null
        if [ $? == 0 ]; then
            loginfo "User seems to be authenticated to ${1}"
            AUTHED=1
        else
            lognotice "User is not authenticated to ${1}"
        fi
    else
        loginfo "No ${DOCKER_CONF_PATH} file found."
        lognotice "User is not authenticated to ${1}"
    fi
    return ${AUTHED}
}

# This function checks if docker is authed to registries and tries to login if not.
checkDockerRegistryAuth () {
    isAuthedToRegistry ${REGISTRY_BASE_URL}
    if [ $? == 0 ]; then
        loginfo "Authenticating to Registry : ${REGISTRY_BASE_URL}."
        if [ ${CI_REGISTRY_USER:-"undefined"} == "undefined" ]; then
            docker login ${REGISTRY_BASE_URL}
        else
            loginfo "Welcome Gitlab-CI Runner :D"
            docker login ${REGISTRY_BASE_URL} -u ${CI_REGISTRY_USER} -p ${CI_JOB_TOKEN}
        fi
    fi
}

# Overall processing
main ()
{
    init "$@"
    BRANCH=${CI_COMMIT_BRANCH:-"local"}
    # We are overloading the BRANCH parameter with the env to deploy on.
    BRANCH=`echo ${1:-${BRANCH}} | tr '[:upper:]' '[:lower:]'`
    REPO_NAME=`git config --local remote.origin.url \
            | sed -n 's#.*/\([^.]*\)\.git#\1#p' \
            | tr '[:upper:]' '[:lower:]'`
    REGISTRY_URL=${REGISTRY_GROUP_URL}${REPO_NAME}
    if [[ ${BRANCH} != "master" && ${BRANCH} != "main" ]]; then
        REGISTRY_URL=${REGISTRY_URL}"/${BRANCH}"
    fi

    checkSkopeoPresence
    checkDockerPresence

    if [[ `uname -s` == "Darwin" ]]; then
        # Mac OSX
        MD5_TOOL=md5
        AWK_HASH='4'
    else
        MD5_TOOL=md5sum
        AWK_HASH='1'
    fi

    # if [ ${BRANCH} != "dev" ]; then
    #     WATCH_LIST="${WATCH_LIST} .env"
    #     loginfo "Added .env to the WATCH_LIST"
    # fi

    # Hash md5 of 'updateDockerBase.sh base.Dockerfile package.json package-lock.json'
    FOLDER_CODE_HASH_BASE="$(find ${WATCH_LIST_BASE[*]} -type f -exec ${MD5_TOOL} {} \; \
            | awk -v HASH="$AWK_HASH" "{print $"HASH"}" > tmp-md5-base \
            && ${MD5_TOOL} tmp-md5-base \
            | awk -v HASH="$AWK_HASH" "{print $"HASH"}")"

    # Hash md5 of WATCH_LIST
    FOLDER_CODE_HASH="$(find ${WATCH_LIST[*]} -type f -exec ${MD5_TOOL} {} \; \
            | awk -v HASH="$AWK_HASH" "{print $"HASH"}" > tmp-md5 \
            && ${MD5_TOOL} tmp-md5 \
            | awk -v HASH="$AWK_HASH" "{print $"HASH"}")" 

    # Check if the Base exists
    loginfo "Base is in ${FOLDER_CODE_HASH_BASE} version, checking if it's up to date."
    bash ./scripts/updateDockerBase.sh
    if [ $? == 1 ]; then
        exit 1;
    fi
    echo "---"

    PROJECT_VERSION=${FOLDER_CODE_HASH_BASE}-${FOLDER_CODE_HASH}
    # echo "PROJECT_VERSION=${PROJECT_VERSION}" > project-version.env

    checkDockerRegistryAuth
    # Check if this project version has already an image
    loginfo "Project is in ${PROJECT_VERSION} version, checking if it's up to date."
    skopeo inspect docker://${REGISTRY_URL}:${PROJECT_VERSION} 1>/dev/null 2>/dev/null

    # Build the Project if not found on registry
    if [ $? != 0 ]; then
        if [[ ${BRANCH} == "master" || ${BRANCH} == "main" ]]; then
            DOCKERFILE="prod.Dockerfile"
        else
            DOCKERFILE="dev.Dockerfile"
        fi
        lognotice "Hash of Project not found on registry, building a new one with ${DOCKERFILE} on :"
        lognotice "Project Version ${PROJECT_VERSION}, latest."
        docker build . \
            -f ./docker/${DOCKERFILE} \
            --tag ${REGISTRY_URL}:${PROJECT_VERSION} \
            --tag ${REGISTRY_URL}:latest
        if [ $? == 0 ]; then
            loginfo "Pushing new images to registry."
            loginfo "Pushing ${REGISTRY_URL}:${PROJECT_VERSION}"
            docker push ${REGISTRY_URL}:${PROJECT_VERSION}
            if [ $? == 0 ]; then
                loginfo "Pushing ${REGISTRY_URL}:latest"
                docker push ${REGISTRY_URL}:latest
            else
                logerror "Docker push of project image failed, exiting the script now."
                exit 1
            fi
        else
            logerror "Docker build of project image failed, exiting the script now."
            exit 1
        fi
    else
        loginfo "The docker project image is already up to date : ${PROJECT_VERSION}."
        loginfo "Trying to pull latest to make sure we got it locally with the latest tag."
        docker pull ${REGISTRY_URL}:latest 2>/dev/null
    fi
    exit 0
}

# script entry point
main "$@"
