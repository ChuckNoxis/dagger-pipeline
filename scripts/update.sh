#!/usr/bin/env bash

# This function initializes the system prior to main processing
init () {
    set -o nounset    # Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
    ulimit -c 0       # Set Core size to 0

    # Call the function cleanupBeforeExit when the signal EXIT is catched
    trap "cleanupBeforeExit" EXIT
}

# This function is called before exiting the program
cleanupBeforeExit () {
    loginfo "update.sh Done"
}

# Format strings for logfunctions
format () {
    echo "$(date +"%d/%m/%y %T") [${1}]";
}

loginfo ()  { echo -e "\033[32m$(format INFO)\033[0m ${@}"; }
lognotice ()  { echo -e "\033[34m$(format NOTICE)\033[0m ${@}"; }
logwarning () { echo -e "\033[33m$(format WARNING)\033[0m ${@}"; }
logerror () { echo -e "\033[31m$(format ERROR)\033[0m ${@}" 1>&2; }

# Overall processing
main ()
{
    init "$@"
    BRANCH=${1:-"local"}

    loginfo "Updating script is trying to pull the last version of the branch : ${BRANCH}."
    git fetch
    git checkout ${BRANCH}
    if [ $? == 1 ]; then
	logwarning "Branch ${BRANCH} not found, aborting script."
	exit 1
    fi
    git reset --hard HEAD
    git pull
}

# script entry point
main "$@"
