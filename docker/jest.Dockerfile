# ---- Release based on Dockerfile-Base ----
FROM registry.gitlab.com/chucknoxis/dagger-pipeline/base:latest

# Setting environment to docker by default,
# you can change it at build time with --build-arg
ARG environment=test
ENV NODE_ENV=${environment}
ENV CI=true

# set the working directory
WORKDIR /app

# Import app sources and tests
COPY src /app/src
COPY jest.config.js /app
COPY tests /app/tests

CMD [ "npm", "run", "jest" ]
