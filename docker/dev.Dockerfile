# ---- Release based on Dockerfile-Base ----
FROM registry.gitlab.com/chucknoxis/dagger-pipeline/base:latest

# Setting environment to docker by default,
# you can change it at build time with --build-arg
ARG environment=development
ENV NODE_ENV=${environment}

# expose port and set the working directory
EXPOSE 3000
WORKDIR /app

# Import app sources
COPY src /app/src

# Checks every 20s if the page loads within 3 seconds
# HEALTHCHECK --interval=20s --timeout=3s --retries=3 \
# 	     CMD curl --fail http://localhost:3000/api/public/health || exit 1

CMD [ "npm", "run", "nodemon" ]
