const controllers = {
  // Route /api/public/test
  test(req, res, next) {
    res.json('test template service');
    next();
  },
  // Route /api/public/health
  async health(req, res, next) {
    const healthcheck = {
      uptime: process.uptime(),
      version: process.env.npm_package_version,
      state: 'healthy',
      message: 'OK',
      timestamp: Date.now(),
    };
    res.type('json').send(healthcheck);
    next();
  },
};

module.exports = controllers;
