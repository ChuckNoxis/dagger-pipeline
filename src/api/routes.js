const publicController = require('../controller/publicController');

module.exports = function routes(app) {
  app.route('/api/public/test')
    .get(publicController.test);
  app.route('/api/public/health')
    .get(publicController.health);
};
