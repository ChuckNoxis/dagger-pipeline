/* const mongoose = require('mongoose'); */

const handler = {
  // exitHandler is called before the program stops.
  exitHandler(signal) {
    console.log(`Received ${signal}, starting cleanup to shutdown gracefully.`);
    // Cleaning logic like DB disconnection
    // Can't clean actually as it triggers db.on('disconnected')
    /* if (mongoose.connection.readyState == 1)
       mongoose.connection.close(); */
    console.log(`Cleanup finished, ${process.env.npm_package_name} is shutting down.`);
  },
};

module.exports = handler;
