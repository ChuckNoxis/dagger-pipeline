// REQUIRE
const http = require('http');
const express = require('express');
const helmet = require('helmet');
const dotenv = require('dotenv');
const morgan = require('morgan');
// const ecsFormat = require('@elastic/ecs-morgan-format')();
const bodyParser = require('body-parser');
// const handler = require('./signalHandler');
const routes = require('./api/routes');

// CONFIG UPHILL
dotenv.config();

// DECLARATION
// const signalsToCatch = ['exit', 'uncaughtException'];
const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'local';
const app = express();
const server = http.createServer(app);

// MIDDLEWARE
const logger = morgan('dev');
app.use(logger); // Logs of endpoints with morgan
app.use(helmet());
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // to support URL-encoded bodies

// CONFIG DOWNSTREAM
routes(app);

const httpserver = server.listen(port, () => {
  console.log(`${process.env.npm_package_name} started on port : ${port}`);
  console.log(`Environment : ${env}`);
});

/* process.on('SIGINT', process.exit); // catches ctrl+c
 * signalsToCatch.forEach((signal) => process.on(signal, () => {
 *   handler.exitHandler(signal);
 *   httpserver.close();
 *   server.close();
 *   process.exit();
 * }));
 *  */
module.exports.app = app;
module.exports.httpserver = httpserver;
